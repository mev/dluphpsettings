DluPhpSettings
==============

--------------------------

Introduction
------------

DluPhpSettings is a [Zend Framework 2](http://framework.zend.com/zf2) module used to set the PHP ini settings based
on the configuration data.

More info
---------
The module was loaded to the repository, because the original repository of the module on bitbucket was deleted.

Installation - with Composer
----------------------------

1.   Go to your project's directory.
2.   Edit your `composer.json` file and add following code into `repositories` section.
```
    {
        "type"   : "package",
        "package": {
            "name"   : "mev/dluphpsettings",
            "version": "0.2",
            "source" : {
                "url"      : "git@bitbucket.org:mev/dluphpsettings.git",
                "type"     : "git",
                "reference": "master"
            },
            "require": {
                "php": ">=5.4.0",
                "zendframework/zendframework": ">=2.0.5"
            },
            "autoload": {
                "classmap": [
                    "./Module.php"
                ]
            }
        }
    }
```  
__Notice:__ Be sure that you remove old package 'dlu/dluphpsettings', because `composer require` will fail.
  
3.   Run `php composer.phar require mev/dluphpsettings`.

Usage
-----

Put any allowed PHP ini settings under the `phpSettings` key into any of your configuration files (local.php or global.php):

    /* Global application configuration in /config/autoload/global.php */
    <?php
    return array(
        'phpSettings'   => array(
            'display_startup_errors'        => false,
            'display_errors'                => false,
            'max_execution_time'		    => 60,
            'date.timezone'                 => 'Europe/Prague',
            'mbstring.internal_encoding'    => 'UTF-8',
        ),
    );

You can find the [list of available PHP ini directives](http://www.php.net/manual/en/ini.list.php) in the PHP docs.

Use the global config files for global application configuration and local config files for settings relevant only
to the local environment.

Links
-----

- [DluPhpSettings](https://bitbucket.org/mev/dluphpsettings) - git repository on BitBucket
